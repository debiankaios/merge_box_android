This work is licensed under a [Creative Commons Attribution-ShareAlike 3.0 Unported License](https://creativecommons.org/licenses/by-sa/3.0/).

This Programm was developed with [Godot Engine.](https://godotengine.org) 

Copyright (c) 2021 Merge Box contributors.
